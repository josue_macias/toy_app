class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
=begin
Once the @users variable is defined, the controller calls the view. 
Variables that start with the @ sign, called instance variables, are automatically available in the views; 
in this case the index.html.erb  
=end
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
      #@microposts = Microposts.find(params[:user_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email)
    end
end

=begin

you may notice that there are more actions than there are pages; the index, show, new, and edit actions all correspond to pages 
from Section 2.2.1, but there are additional create, update, and destroy actions as well. 
These actions don’t typically render pages (although they can); instead, their main purpose is to modify information about 
users in the database. This full suite of controller actions, summarized in Table 2.2, 
represents the implementation of the REST architecture in Rails (Box 2.2), 
which is based on the ideas of representational state transfer identified and named by computer scientist Roy Fielding.
for example, both the user show action and the update action correspond to the URL /users/1. 
The difference between them is the HTTP request method they respond to. We’ll learn more about HTTP request methods 
starting in Section 3.3.


HTTP request	*** URL	*** Action	*** Purpose
GET	 ***  /users	***  index	*** page to list all users
GET	***  /users/1	***  show	*** page to show user with id 1
GET	***  /users/new	***  new	*** page to make a new user
POST	***  /users	***  create	*** create a new user
GET	***  /users/1/edit	***  edit	*** page to edit user with id 1
PATCH	***  /users/1	*** update	*** update user with id 1
DELETE	***  /users/1	*** destroy	*** delete user with id 1


“REST” is an acronym for REpresentational State Transfer.
REST is an architectural style for developing distributed, networked systems and 
software applications such as the World Wide Web and web applications.
the four fundamental HTTP request methods: POST, GET, PATCH, and DELETE.

As a Rails application developer, the RESTful style of development helps you make choices 
about which controllers and actions to write: you simply structure the application using 
resources that get created, read, updated, and deleted.

=end