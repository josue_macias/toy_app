class User < ApplicationRecord
  has_many :microposts
  validates :email, presence: true 
  validates :name, presence: true
end

=begin

The User model for the toy application:

the line @users = User.all (located in users_controller.rb in the index action), which asks the User model (this file) to retrieve a list of all the users 
from the database, and then places them in the variable @users (pronounced “at-users”). 
Although it is rather plain, it comes equipped with a large amount of functionality because of inheritance. 
In particular, by using the Rails library called Active Record, the code arranges for User.all to return all the users in the database.

=end