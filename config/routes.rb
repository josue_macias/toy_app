Rails.application.routes.draw do
  resources :microposts
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'users#index' # In the present case, we want to use the index action in the Users controller.
  
end
=begin
The code to create the mapping of user URLs to controller actions for the Users resource is: "resources :users"
This code effectively sets up the table of URL/action pairs:

The correspondence between pages and URLs for the Users resource:
URL	           Action	   Purpose
/users	       index	   page to list all users
/users/1	     show	     page to show user with id 1
/users/new	   new	     page to make a new user
/users/1/edit	 edit	     page to edit user with id 1
=end